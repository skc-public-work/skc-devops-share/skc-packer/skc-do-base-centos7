<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [skc-do-base-centos7](#skc-do-base-centos7)
- [useful notes](#useful-notes)
    - [pass variables](#pass-variables)
- [packer commands](#packer-commands)

<!-- markdown-toc end -->



# skc-do-base-centos7

- [ ] packer build of a base centos 7 system pushed to DigitalOcean


# useful notes
## pass variables
- [ ] pass variables

https://www.packer.io/guides/hcl/variables


# packer commands

- [ ] validate
```
packer validate .
```

- [ ] build
```
packer build .
```
