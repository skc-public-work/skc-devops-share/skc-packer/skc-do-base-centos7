
variable "api_token" {
  type    = string
  default = "${env("TF_VAR_do_api_key")}"
}

variable "region" {
  type    = string
  default = "nyc3"
}

variable "drop_size"{
  type    = string
  default = "s-2vcpu-2gb"
}

variable "sn_name"{
  type    = string
}




source "digitalocean" "skc-do-build" {
  api_token     = "${var.api_token}"
  image         = "centos-7-x64"
  region        = "${var.region}"
  size          = "${var.drop_size}"
  snapshot_name = "${var.sn_name}"
  ssh_username  = "root"
  tags          = ["skc-base-centos7"]
}


build {
  sources = ["source.digitalocean.skc-do-build"]

  provisioner "ansible" {
    playbook_file = "../ansible/build.yml"
  }

}
